## P2000 Raspberry PI
This is the source code of the P2000 scanner that I ran on my Raspberry PI 3. 
The project is currently abandoned as I just wanted to try this out.

**prev.py**: This starts the P2000 scanner and executes post.py everytime a new call is received. <br>
**post.py**: This file posts the raw data to my [API](https://gitlab.com/Michel3951/p2000-php) in which it is decoded and stored. 