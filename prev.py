import subprocess

process = subprocess.Popen("rtl_fm -f 169.65M -M fm -s 22050 | multimon-ng -a FLEX -t raw /dev/stdin", stdout=subprocess.PIPE, shell=True)

while True:
  line = process.stdout.readline()
  if not line:
    break

  message = line.rstrip()
  body = {'message': message}
  print(body)